function [integration, count] = trapezoid(a, b, eps)

h=b-a;
old = 0;
n=0;
new = h/2*(f(a)+f(b));
%count times
count=2;

while abs(new-old) > eps
    n = n + 1;
    old = new;
    new = 0;
    h = h/2;
    %summation
    for i=1:2^(n-1)
        new = new + f(a+(2*i-1)*h);
        count = count + 1;
    end
    new = old/2 + h*new;
end

integration = new;

