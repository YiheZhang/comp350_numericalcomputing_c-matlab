function numI = simpson(a, b, eps, level, level_max)
h = b-a;
c = (a+b)/2;
I1 = h*(f(a)+4*f(c)+f(b))/6;

%divider to next level
level = level+1;
d = (a+c)/2;
e = (c+b)/2;
I2 = h*(f(a)+4*f(d)+2*f(c)+4*f(e)+f(b))/12;

global count2;
count2 = count2 + 2;

if level >= level_max
    numI = i2;
else
    if abs(I2-I1) <= (15*eps)
        %final approximation
        numI = I2 + (I2-I1)/15;
    else
        numI = simpson(a,c,eps/2,level,level_max) + simpson(c,b,eps/2,level,level_max);
    end
end