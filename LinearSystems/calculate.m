function [x] = calculate(A,b)
%This funciton outputs the computed x from the Ax = b
[L,U,P] = lupp(A);
t = P * b;
n = size(A,1);

Ux = zeros(n,1);
Ux(1,1) = t(1,1);
for k = 2:n
    s = 0;
    for i = 1:k-1
        s = s + L(k,i)*Ux(i,1);
    end
    Ux(k,1) = t(k,1) - s;
end
x = zeros(n,1);
x(n,1) = Ux(n,1)/U(n,n);
for k = n-1:-1:1
    s = 0;
    for i = n:-1:k+1
        s = s + U(k,i)*x(i,1);
    end
    x(k,1) = (Ux(k,1) - s)/U(k,k);
end