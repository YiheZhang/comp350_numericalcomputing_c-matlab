%This function reqires getInv.m and lupp.m
function [] = q3c()

A = zeros(5,5);
% A = (aij), aij = 1/(i + j ? 1);
for i = 1:5
    for j = 1:5
        A(i,j) = 1/(i+j-1);
    end
end

[L,U,P] = lupp(A);
%A^-1 = U^-1 * L^-1 * P
getInv(U)*getInv(L)*P