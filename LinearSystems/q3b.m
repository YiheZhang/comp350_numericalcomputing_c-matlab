%This function requires lupp.m, getInv.m
function [] = q3b()
A = zeros(10,10);
% A = (aij), aij = 1/(i + j ? 1);
for i = 1:10
    for j = 1:10
        A(i,j) = 1/(i+j-1);
    end
end

%loop 10 times
for i = 1:10
   Xt = randn(10,5);
   b = A * Xt;
   
   [L,U,P] = lupp(A);
   Xc = zeros(size(A,1),size(b,2));
   for k = 1:size(b,2)
       [Xc(:,k)] = calculate(A,b(:,k));
   end
   
   i
   norm(Xc-Xt,'fro')/norm(Xt,'fro')
   eps*norm(A,'fro')*norm(getInv(U)*getInv(L)*P,'fro')
   norm(b-A*Xc,'fro')/(norm(A,'fro')*norm(Xc,'fro'))
end