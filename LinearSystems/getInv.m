%input a matrix L/U, and it returns the inverse of L/U
function [Y] = getInv(X)
%Create an indentity matrix with the size of X
Y = eye(size(X,1));
%calculate the inverse of U
for k = size(X,1):-1:1
    Y(k,:) = Y(k,:)/X(k,k);
    for i = k-1:-1:1
        mult = X(i,k);
        Y(i,:) = Y(i,:)-mult*Y(k,:);
    end
end
%calculate the inverse of L
for k = 1:size(X,1)
    for i = k+1:size(X,1)
        mult = X(i,k)/X(k,k);
        Y(i,:) = Y(i,:)-mult*Y(k,:);
    end
end
    

