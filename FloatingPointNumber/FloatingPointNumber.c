#include <stdio.h>
#include <math.h>

//Question 1 Single
void q1Single() {
    printf("\nQuestion 1 single precision\n");
    float x = 1, temp, smallest=1;

    while (x==smallest) {
        x++;
        temp = 1/x;
        smallest = 1/temp;
    }
    printf("Q1 single precision smallest = %e \n", smallest);
}

//Question 1 Double
void q1Double() {
    printf("\nQuestion 1 double precision\n");
    double x = 1, temp, smallest=1;

    while (x==smallest) {
        x++;
        temp = 1/x;
        smallest = 1/temp;
    }
    printf("Q1 double precision smallest = %e \n", smallest);
}

//Question 2
void q2() {
    printf("\nQuestion 2\n");
    float x = 100;

    for (int i=2; i<=70; i++) {
        x = 100*x/i;
        printf("n=%i, result =  %e \n", i, x);
    }
}

//Question 2 Bonus
void q2Bonus() {
    printf("\nQuestion 2 bonus\n");
    double x = 100;

    for (int i=2; i<=70; i++) {
        x = 100*x/i;
        printf("n=%i, result =  %e \n", i, x);
    }
}

//Question 3a
void q3a() {
    printf("\nQuestion 3a\n");
    double p=2*sqrt(2), temp, temp2;

    for (double i=3; i<=35; i++) {
        temp = pow(2, i-1);
        temp2 = sqrt(1-pow(p/temp, 2));
        temp2 = sqrt(2*(1-temp2));
        p = temp * temp2;
        printf("n=%1.0f, result=%e \n", i, p);
    }
}

//Question 3b
void q3b() {
    printf("\nQuestion b\n");
    double p=2*sqrt(2), q = pow(p/pow(2, 1), 2), temp;

    for (double i=3; i<=35; i++) {
        temp = pow(2, i-1);
        q = q/(2+sqrt(4-q));
        p = temp * sqrt(q);
        printf("n=%1.0f, result=%e \n", i, p);
    }
}

int main() {
    q1Single();
    q1Double();
    q2();
    q2Bonus();
    q3a();
    q3b();
}