%plot graph
x = -1:0.01:2;
y = x.^3 - 5*x + 3;
plot(x,y);