function root = secant(fname,x0,x1,xtol,ftol,nmax)
% Secant Method.
%
% input:  fname is a string that names the function f(x).
%         xo and x1 are two initial points
%         xtol and ftol are termination tolerances
%         n_max is the maximum number of iteration
%         display = 1 if step-by-step display is desired,
%                 = 0 otherwise
% output: root is the computed root of f(x)=0
%

fx0 = feval(fname,x0);
fx1 = feval(fname,x1);

disp('           x0             x1                    f(x0)                    f(x1)')
disp(sprintf('%23.15e %23.15e %23.15e %23.15e', x0, x1, fx0, fx1))
if abs(fx1) <= ftol
   root = x1;
   return
end

for n = 1:nmax
    d = (x1-x0)*fx1/(fx1-fx0);
    x0 = x1;
    fx0 = fx1;
    x1 = x1-d;
    fx1 = feval(fname,x1);
    disp(sprintf('%23.15e %23.15e %23.15e %23.15e', x0, x1, fx0, fx1))
    if abs(d)<=xtol || abs(feval(fname,x0))<=ftol
        root = x1;
        return
    end
end
root = c;