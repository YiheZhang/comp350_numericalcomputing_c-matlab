function root = newton(fname,fdname,x,xtol,ftol,nmax)

fx = feval(fname,x);

%display
disp('            x                    f(x)')
disp(sprintf('%23.15e %23.15e', x, fx))

if abs(fx) <= ftol
   root = x;
   return
end
for n = 1:nmax
    fdx = feval(fdname,x);
    d = fx/fdx;
    x = x - d;
    fx = feval(fname,x);
    %display
    disp(sprintf('%23.15e %23.15e', x, fx))
    if abs(d) <= xtol || abs(fx) <= ftol
       root = x;
       return
    end
end
root = x;