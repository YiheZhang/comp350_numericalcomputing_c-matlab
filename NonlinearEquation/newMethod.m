function root = newMethod(fname,fdname,fddname,x,xtol,ftol,nmax)

fx = feval(fname,x);

%display
disp('            x                    f(x)')
disp(sprintf('%23.15e %23.15e', x, fx))

if abs(fx) <= ftol
   root = x;
   return
end
for n = 1:nmax
    fdx = feval(fdname,x);
    fddx = feval(fddname,x);
    d = 2*fx*fdx/(2*fdx.^2-fddx*fx);
    x = x - d;
    fx = feval(fname,x);
    %display
    disp(sprintf('%23.15e %23.15e', x, fx))
    if abs(d) <= xtol || abs(fx) <= ftol
       root = x;
       return
    end
end
root = x;