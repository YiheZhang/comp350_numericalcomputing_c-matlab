function root = bisection(fname,a,b,delta)

fa = feval(fname,a); 
fb = feval(fname,b);
if sign(fa)*sign(fb) > 0
   disp('function has the same sign at at a and b')
   return
end
if fa == 0
   root = a;
   return
end  
if fb == 0
   root = b;
   return
end
c = (a+b)/2;
fc = feval(fname,c);
ebound = abs(b-a)/2;

%display
disp('      a              b               c               f(c)            error_bound');
disp(sprintf('%15.5e %15.5e %15.5e %15.5e %15.5e', a, b, c, fc, ebound))

while ebound > delta
   if fc == 0
      root = c;
      return
   end   
   if sign(fa)*sign(fc) < 0
      b  = c;
      fb = fc;
   else
      a  = c;
      fa = fc;
   end
   c = (a+b)/2;
   fc = feval(fname,c);
   ebound = ebound/2;
   disp(sprintf('%15.5e %15.5e %15.5e %15.5e %15.5e', a, b, c, fc, ebound))
end
root = c;