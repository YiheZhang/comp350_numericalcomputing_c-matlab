function z = cubicSpline(t,y,n)

h = zeros(n,1);
b = zeros(n,1);
for i = 1: n
    h(i) = t(i+1) - t(i);
    b(i) = (y(i+1) - y(i)) / h(i);
end

%Forward elimination
u = zeros(n-1,1);
v = zeros(n-1,1);

u(1) = 2 * (h(1) + h(2));
v(1) = 6 * (y(2) - y(1));
for i = 3 : n
    mult = h(i-1) / u(i-2);
    u(i-1) = 2 * (h(i-1) + h(i)) - mult * h(i-1);
    v(i-1) = 6 * (b(i) - b(i-1)) - mult * v(i-2);
end

%Back substitution
z = zeros(n+1,1);
for i = n : -1 : 2
    z(i) = (v(i-1) - h(i) * z(i+1)) / u(i-1);
end
